

contract hello {
        address public owner;
        string public publicKey;
        string[][] public contracts;
        int public price;

        
        function hello() {
                owner = msg.sender;
                publicKey = "236a6890cf9e0b1e30392";
                price = -1;
        }
        
        function setPrice(string carAge, string engineSize, string under25) {
                /*
                int age = Integer.parseInt(carAge);
                int size = Integer.parseInt(engineSize);
                int u25 = Integer.parseInt(under25);
                price = 78 + 39*age + size*12 + u25*450; */
                price = 2344545;
        }

        function getPrice() constant returns (int p) {
                p = price;
        }
        
        function signContract(string carAge, string engineSize, string under25) {
                contracts.push([carAge, engineSize, under25]);
        } 

        function() {
                // This function gets executed if a
                // transaction with invalid data is sent to
                // the contract or just ether without data.
                // We revert the send so that no-one
                // accidentally loses money when using the
                // contract.
                throw;
        }
}