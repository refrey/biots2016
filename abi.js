contractAbi = [
  {
    "constant": false,
    "inputs": [
      {
        "name": "carAge",
        "type": "string"
      },
      {
        "name": "engineSize",
        "type": "string"
      },
      {
        "name": "under25",
        "type": "string"
      }
    ],
    "name": "setPrice",
    "outputs": [],
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "getPrice",
    "outputs": [
      {
        "name": "p",
        "type": "int"
      }
    ],
    "type": "function"
  }
]