var express = require('express');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

var Web3 = require('web3');
var sandboxId = '7d8aced889';
var sandboxUrl = 'https://remo.by.ether.camp:8555/sandbox/' + sandboxId;
var web3 = new Web3(new Web3.providers.HttpProvider(sandboxUrl));
var contractAddress = '0x052ecb441e56e28803008f70daa586959797f937';
require('./abi.js');
console.log(JSON.stringify(contractAbi));
web3.eth.defaultAccount = web3.eth.accounts[0];

var contractObject = web3.eth.contract(contractAbi);
var contractInstance = contractObject.at(contractAddress);

var path = require("path");

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/test.html'));
   //res.send('Hello World');
})

app.get('/getBlock', function (req, res) {
   res.send('blockCount: ' + web3.eth.blockNumber);
})

app.post('/setPrice', function (req, res, next) {
    console.log(req.body.B);
    contractInstance.setPrice(req.body.A,req.body.B,req.body.C);
    res.send('<a href="https://remo.by.ether.camp:8080/getPrice">getPrice</a>');
})

app.get('/getPrice', function (req, res) {
    contractInstance.getPrice(function(error, response) {
        console.log('price: ' + response + ', error: ' + error);
    });
    res.send(contractInstance.getPrice().toString());
})


var server = app.listen(8080, function () {
  console.log("express server running");
  console.log('default account: ' + web3.eth.accounts[0])
})


